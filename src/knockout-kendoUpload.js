createBinding({
    name: "kendoUpload",
    watch: {
        enabled: ENABLE
    },
    templates: ["template"],
    altTemplateContext: function(widget) {
        return widget.wrapper[0];
    }
});