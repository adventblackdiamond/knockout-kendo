createBinding({
    name: "kendoFlatColorPicker",
    events: {
        change: VALUE
    },
    watch: {
        enabled: ENABLE,
        value: VALUE,
        color: VALUE,
        palette: PALETTE
    }
});