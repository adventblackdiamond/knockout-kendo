createBinding({
    name: "kendoTreeList",

    watch: {
        data: function (value) {
            ko.kendo.setDataSource(this, value);
        }
    },
    templates: ["rowTemplate", "altRowTemplate"]
});