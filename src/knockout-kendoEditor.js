createBinding({
    name: "kendoEditor",
    defaultOption: VALUE,
    events: {
        change: VALUE,
        keyup: KEYUP,
    },
    watch: {
        enabled: ENABLE,
        value: VALUE
    }
});